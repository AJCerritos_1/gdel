<?php 
class Usuario extends Eloquent { //Todos los modelos deben extender la clase Eloquent
    public $timestamps = false;
    protected $table = 'usuarios';
    protected $primaryKey = 'idUsuario';
    
    protected $fillable = ['nickname', 'pass', 'isAdmin'];
}
?>