@extends('layouts.master')
 
@section('sidebar')
     @parent
     Lista de usuarios
@stop
 
@section('content')
	<div class="row">
		
			<div id="CarouselPrincipal" class="carousel slide">
			  <!-- Indicators -->
			  <ol class="carousel-indicators">
				<li data-target="#CarouselPrincipal" data-slide-to="0" class="active"></li>
				<li data-target="#CarouselPrincipal" data-slide-to="1"></li>
				<li data-target="#CarouselPrincipal" data-slide-to="2"></li>
			  </ol>
			  <div class="carousel-inner">
				<div class="item active">
				  {{ HTML::image('images/logo_2.jpg', 'Responsive image', array('class' => 'img-responsive')) }}
				  <div class="container">
					<div class="carousel-caption">
						<h1>Fácil, Creativo, Soluciones Efectivas</h1>
						<pthis is="" an="" example="" layout="" with="" carousel="" that="" uses="" the="" bootstrap="" 3="" styles.<="" small=""><p></p>
							<p>¡TU GUÍA HACIA EL ÉXITO EMPRESARIAL Y PERSONAL!</p>
							<p><a class="btn btn-xs btn-primary" href="#">más...</a></p>
						</pthis>
					</div>
				  </div>
				</div>
				<div class="item">
				  <img src="http://lorempixel.com/1500/600/abstract/1" class="img-responsive">
				  <div class="container">
					<div class="carousel-caption">
					  <h1>Changes to the Grid</h1>
					  <p>Bootstrap 3 still features a 12-column grid, but many of the CSS class names have completely changed.</p>
					  <p><a class="btn btn-large btn-primary" href="#">Learn more</a></p>
					</div>
				  </div>
				</div>
				<div class="item">
				  <img src="http://placehold.it/1500X500" class="img-responsive">
				  <div class="container">
					<div class="carousel-caption">
					  <h1>Percentage-based sizing</h1>
					  <p>With "mobile-first" there is now only one percentage-based grid.</p>
					  <p><a class="btn btn-large btn-primary" href="#">Browse gallery</a></p>
					</div>
				  </div>
				</div>
			  </div>
			  <!-- Controls -->
			  <a class="left carousel-control" href="#CarouselPrincipal" data-slide="prev">
				<span class="icon-prev"></span>
			  </a>
			  <a class="right carousel-control" href="#CarouselPrincipal" data-slide="next">
				<span class="icon-next"></span>
			  </a>  
		</div>
		
	</div>
@stop