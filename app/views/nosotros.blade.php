@extends('layouts.master')

@section('content')
	<div class="row">
		<div class="col-md-8">
			{{ HTML::image('images/nosotros.jpg', '',array('class' => 'img-responsive img-rounded')) }}
		</div>
		<div class="col-md-4">
			<h2>Grupo Empresarial de Desarrollo y Liderazgo, S.A. de C.V.</h2>
			<p>Se dedica a administrar en el cambio dentro y fuera de las organizaciones y empresas con la finalidad de desechar esquemas del pasado. Colaboramos con las empresas en donde la carencia de valores éticos, morales y laborales, provoca el incumplimiento de objetivos y metas, frenando su crecimiento. Nuestras  principales fortalezas son la innovación y el trato personalizado, utilizando diferentes herramientas para lograr los objetivos propuestos. </p>
			<P>Nos  enfocamos  en todo tipo de empresas, pequeñas, medianas, y empresas altamente competitivas en el mercado nacional e internacional, que busquen un continuo mejoramiento en diversos puntos estratégicos de su crecimiento y desarrollo empresarial.</P>
			<a class="btn btn-primary btn-lg RedStyle" href="#" data-toggle="modal" data-target=".historiaModal">Historia!</a>
		</div>
	</div>

    <hr>

	<div class="row">
		<div class="col-lg-12">
			<div class="well text-center">
				<strong>Un hombre con ideas es fuerte, pero un hombre con ideales es invencible.</strong>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4">
			{{ HTML::image('images/mision.jpg', '', array('class' => 'img-circle img-responsive')) }}
			<h2 class="text-center">MISIÓN</h2>
			<p class="text-center">Administrar el cambio en las empresas de nuestros clientes, involucrándonos  directamente  en  la  implantación  de sistemas que faciliten su competitividad, calidad y estabilidad, contribuyendo así al desarrollo de la empresa y de México.
</p>
		</div>
		<div class="col-md-4">
			{{ HTML::image('images/vision.jpg', '', array('class' => 'img-circle img-responsive')) }}
			<h2 class="text-center">VISIÓN</h2>
			<p class="text-center">consultoría con una posición sólida y competitiva en el mercado nacional e internacional, a través de la incorporación constante de nuevas técnicas de negocio, que satisfagan en forma oportuna las necesidades presentes y futuras de nuestros clientes, con personal actualizado e innovado.

</p>
		</div>
		<div class="col-md-4">
			{{ HTML::image('images/valores.jpg', '', array('class' => 'img-circle img-responsive')) }}
			<h2 class="text-center">VALORES</h2>
			<p class="text-center">Honestidad</p>
			<p class="text-center">Creatividad</p>
			<p class="text-center">Lealtad</p>
			<p class="text-center">Espíritu laboral</p>
		</div>
	</div>

		<!--Modals-->
		<div class="modal fade historiaModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						{{ HTML::image('images/logo_2.jpg', 'Responsive image', array('class' => 'img-responsive img-rounded', 'style' => 'width: 2em;display: inline-block;')) }}
						<h4 class="modal-title" id="myModalLabel" style="display: inline-block;">HISTORIA. GDEL, S.A DE C.V.</h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<p>Nace en el año 2006 como respuesta a la vertiginosa falta de valores laborales y  
								profesionales que existe en el mundo empresarial. Además, nace con la idea de transformar nuestras  
								empresas mexicanas en empresas altamente competitivas a nivel internacional, impulsando a nuestro país 
								a una mejora continua en los diferentes rangos de crecimiento laboral, desempeño profesional exitoso y 
								crecimiento y explotación de los valores morales y éticos más preciados para cada uno de los mexicanos; impactando de manera proactiva en las utilidades de cada una de las empresas allegadas a este programa.
							</p>
							<p>El modelo GDEL se desprende de la iniciativa que el empresario Jairo Darío 
							Castro Escutia, tuvo hace varios años de perseguir un cambio que reconstruya  
							la misión y visión con acciónes contundentes en las empresas contemporáneas y 
							así impactar positivamente en nuestra sociedad actual forjando un panorama digno 
							para el futuro laboral de nuestro país</p>
						</div>
					</div>
			</div>
		  </div>
		</div>
		
@stop