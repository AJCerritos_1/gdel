@extends('layouts.master')

@section('content')
		
		<div class="row panel panel-default well well-sm col-md-offset-1 col-md-10">
			<div class="col-md-offset-1 col-md-5">
                <div class="panel panel-default">
                    <div class="text-center header">Dirección</div>
                    <div class="panel-body text-center">
                        <address>
							<strong>Gral. Joaquin Musel 14-102</strong><br />
							Lomas del Huizachal<br />
							Naucalpan, Edo. Mex.<br />
							México 53840<br />
                        </address>
                        <hr />
						<address>
							<abbr title="Phone">Teléfono:</abbr> (55)52940677 <br />
							<strong></strong><br />
							<a href="#">gdel@gdel.com.mx</a><br />
							<a href="#">atencionaclientes@gdel.com.mx</a><br />
							<a href="#">ventas@gdel.com.mx</a>
						</address>
                        <div id="map1" class="map">
                        </div>
                    </div>
                </div>
			</div>
			<div class="col-md-5">
				<div class="panel panel-default">
					<form class="form-horizontal" method="post">
						<fieldset>
							<legend class="text-center header">Envíanos un mensaje...</legend>
							<div class="form-group">
								<div class="col-xs-10 col-sm-10 col-md-10 col-xs-offset-1 col-sm-offset-1 col-md-offset-1">
									<input id="fname" name="name" type="text" placeholder="Nombre" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<div class="col-xs-10 col-sm-10 col-md-10 col-xs-offset-1 col-sm-offset-1 col-md-offset-1">
									<input id="lname" name="name" type="text" placeholder="Teléfono" class="form-control">
								</div>
							</div>

							<div class="form-group">
								<div class="col-xs-10 col-sm-10 col-md-10 col-xs-offset-1 col-sm-offset-1 col-md-offset-1">
									<input id="email" name="email" type="text" placeholder="E-mail" class="form-control">
								</div>
							</div>

							<div class="form-group">
								<div class="col-xs-10 col-sm-10 col-md-10 col-xs-offset-1 col-sm-offset-1 col-md-offset-1">
									<textarea class="form-control" id="message" name="message" placeholder="Mensaje" rows="7"></textarea>
								</div>
							</div>

							<div class="form-group">
								<div class="col-xs-offset-4 col-sm-offset-7 col-md-offset-5 col-xs-2 col-sm-2 col-md-2 text-center">
									<button type="submit" class="btn btn-primary btn-md">Borrar</button>
								</div>
								<div class="col-xs-offset-1 col-md-offset-1 col-xs-2 col-sm-2 col-md-2 text-center">
									<button type="submit" class="btn btn-primary btn-md">Enviar</button>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
    
@stop