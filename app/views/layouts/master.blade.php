<!DOCTYPE html>
<html>
    <head>
        <title>
            @section('title')
            GDEL: Asesoría y Consultoría Empresarial, Soluciones de la más alta calidad para tu empresa: Atrévete a hacer el cambio
            @show
        </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<link rel="shortcut icon" href="images/logo_3.png" />
        <!-- CSS are placed here -->
        {{ HTML::style('css/bootstrap.css') }}
        {{ HTML::style('css/bootstrap-theme.min.css') }}
		{{ HTML::style('css/jquery-ui.min.css') }}
		{{ HTML::style('css/jquery-ui.structure.min.css') }}
		{{ HTML::style('css/jquery-ui.theme.min.css') }}
		{{ HTML::style('css/main.css') }}

        <style>
        @section('styles')
            
        @show
        </style>
    </head>

    <body>
    	<div class="container">
	        <!-- Navbar -->
			<div class="row">
				<div>
					{{ HTML::image('images/header.png', 'Responsive image', array('class' => 'img-responsive', 'style' => 'padding-left: 1em;padding-right: 2em;')) }}
				</div>
			</div>
			<div class="col-lg-12 col-md-12" id="barra">
				<nav role="navigation" class="navbar navbar-inverse" style="margin-top: 1em;">
				<div class="navbar-header hidden-md hidden-sm">
					<button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					{{ HTML::link('/', 'GENERAR', array('class' => 'navbar-brand')); }}
				</div>
				<div id="navbarCollapse" class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li class="">{{ HTML::link('nosotros', 'NOSOTROS'); }}</li>
						<li class="dropdown">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">SERVICIOS <b class="caret"></b></a>
							<ul role="menu" class="dropdown-menu">
								<li>{{ HTML::link('asesoria_consultoria', 'Asesoría y Consultoría'); }}</li>
								<li>{{ HTML::link('eventos_empresariales', 'Eventos Empresariales'); }}</li>
								<li>{{ HTML::link('programas_liderazgo', 'Programas de Liderazgo Jr.'); }}</li>
								<li>{{ HTML::link('coordinacion_campanias', 'Coordinación de Campañas Políticas'); }}</li>
								<li>{{ HTML::link('executive_bussines', 'Executive Business Travels'); }}</li>
							</ul>
						</li>
						<li class="">{{ HTML::link('articulos', 'ARTÍCULOS'); }}</li>
						<li class="">{{ HTML::link('noticias', 'NOTICIAS'); }}</li>
						<li class="">{{ HTML::link('contacto', 'CONTACTO'); }}</li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
					<!--<li><a href="#">Registrarse |</a></li>-->
					<li><a href="#" data-toggle="modal" data-target=".accesoModal">Entrar</a></li>
				</ul>
				</div>
				</nav>
			</div>


	        <!-- Container -->
			<div class="row">
				<div class="container">

					<!-- Content -->
					@yield('content')

				</div>
			</div>
			
	
		</div>
		
		<!--Footer-->	
			<div id="footer">
				<div class="container">
					<div class="row">
							<div class="col-xs-6 col-sm-6 col-md-6">
								<strong style="color: white;">GDEL</strong><span style="color: white;"> © 2014</span> <a href="#" data-toggle="modal" data-target=".pricavyPolicyModal">Privacy Policy</a>
							</div>
							<div class="col-xs-6 col-sm-6 col-md-6 text-right">
								<address style="color: whitesmoke;">
									<strong>Sitio Web: </strong>
									<a href="mailto:#">www.gdel.com.mx</a>
								</address>
							</div>
						</div>
				</div>
			</div>
			
		<!--modals-->
			<div class="modal fade accesoModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
			  <div class="modal-dialog modal-sm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						{{ HTML::image('images/logo_2.jpg', 'Responsive image', array('class' => 'img-responsive img-rounded', 'style' => 'width: 2em;display: inline-block;')) }}
						<h4 class="modal-title" id="myModalLabel" style="display: inline-block;">Entrar</h4>
					</div>
					<div class="modal-body">
						  <div class="form-group">
							<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
						  </div>
						  <div class="form-group">
							<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
						  </div>
						  <div class="checkbox">
							<label>
							  <input type="checkbox"> Guarda Contraseña
							</label>
						  </div>
						  {{ HTML::link('usuarios/registro', 'Registrarse'); }}
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						<button type="button" class="btn btn-primary">Entrar</button>
					</div>
				</div>
			  </div>
			</div>
        
		<div class="modal fade pricavyPolicyModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		  <div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						{{ HTML::image('images/logo_2.jpg', 'Responsive image', array('class' => 'img-responsive img-rounded', 'style' => 'width: 2em;display: inline-block;')) }}
						<h4 class="modal-title" id="myModalLabel" style="display: inline-block;">Privacy/Política de Privacidad</h4>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label for="recipient-name" class="control-label">1. Proemio</label>
							<p>Al ingresar y utilziar este portal de internet, ...</p>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="control-label">2. Convenio</label>
							<p>Al ingresar y utilziar este portal de internet, ...</p>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="control-label">3. Licencia</label>
							<p>Al ingresar y utilziar este portal de internet, ...</p>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="control-label">4. Reglas para el uso del sitio web www.gdel.com.mx</label>
							<p>Al ingresar y utilziar este portal de internet, ...</p>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="control-label">5. Formatos</label>
							<p>Al ingresar y utilziar este portal de internet, ...</p>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="control-label">6. Derechos de autor y propiedad industrial</label>
							<p>Al ingresar y utilziar este portal de internet, ...</p>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="control-label">7. Material publico</label>
							<p>Al ingresar y utilziar este portal de internet, ...</p>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="control-label">8. Negación de garantías</label>
							<p>Al ingresar y utilziar este portal de internet, ...</p>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="control-label">9.Limitaciones a las responsabilidades</label>
							<p>Al ingresar y utilziar este portal de internet, ...</p>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="control-label">10. Modificaciones al sitio web www.gdel.com.mx</label>
							<p>Al ingresar y utilziar este portal de internet, ...</p>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="control-label">11. Modificaciones al convenio</label>
							<p>Al ingresar y utilziar este portal de internet, ...</p>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="control-label">12. Términos adicionales</label>
							<p>Al ingresar y utilziar este portal de internet, ...</p>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="control-label">13. Sesión de derechos</label>
							<p>Al ingresar y utilziar este portal de internet, ...</p>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="control-label">14. Indemnización</label>
							<p>Al ingresar y utilziar este portal de internet, ...</p>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="control-label">15. Terminación</label>
							<p>Al ingresar y utilziar este portal de internet, ...</p>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="control-label">16. Subsistencia</label>
							<p>Al ingresar y utilziar este portal de internet, ...</p>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="control-label">17. No renuncia de derechos</label>
							<p>Al ingresar y utilziar este portal de internet, ...</p>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="control-label">18. Legislación de derechos</label>
							<p>Al ingresar y utilziar este portal de internet, ...</p>
						</div>
					</div>
			</div>
		  </div>
		</div>
		
		
		
		<!-- Scripts are placed here -->
        {{ HTML::script('js/jquery-1.11.1.min.js') }}
		{{ HTML::script('js/bootstrap.min.js') }}
		{{ HTML::script('js/jquery-ui.min.js') }}
		{{ HTML::script('http://maps.google.com/maps/api/js?sensor=false') }}
		{{ HTML::script('js/main.js') }}

    </body>
</html>