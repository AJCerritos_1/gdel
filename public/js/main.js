$(document).ready(function(){
	
	//Metodos Jquery
	
	$('#carousel_index').carousel();
	
	$('#trigger').click(function(){
		$("#dialog").dialog();
		$("#dialog").dialog( "option", "width", '65%' );
		$("#dialog").dialog({
			modal: true,
			fluid: true, //new option
			resizable: false
		});
		$("#dialog").parent().css('margin-top','-12em');
		$('#dialog iframe').css('width','100%').css('height','35em');
    }); 
	
	$(document).on("dialogopen", ".ui-dialog", function (event, ui) {
		fluidDialog();
	});
	
	$(window).resize(function () {
		fluidDialog();
	});

	
	// Funciones
	function init_map1() {
            var myLocation = new google.maps.LatLng(38.885516, -77.09327200000001);
            var mapOptions = {
                center: myLocation,
                zoom: 16
            };
            var marker = new google.maps.Marker({
                position: myLocation,
                title: "Property Location"
            });
            var map = new google.maps.Map(document.getElementById("map1"),
                mapOptions);
            marker.setMap(map);
        }
       
	function fluidDialog() {
    var $visible = $(".ui-dialog:visible");
    // each open dialog
    $visible.each(function () {
        var $this = $(this);
        var dialog = $this.find(".ui-dialog-content").data("ui-dialog");
        // if fluid option == true
        if (dialog.options.fluid) {
            var wWidth = $(window).width();
            // check window width against dialog width
            if (wWidth < (parseInt(dialog.options.maxWidth) + 50))  {
                // keep dialog from filling entire screen
                $this.css("max-width", "90%");
            } else {
                // fix maxWidth bug
                $this.css("max-width", dialog.options.maxWidth + "px");
            }
            //reposition dialog
            dialog.option("position", dialog.options.position);
        }
    });

	}
	
	//	llamar funciones
	init_map1();
});